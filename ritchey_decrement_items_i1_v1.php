<?php
#Name:Ritchey Decrement Items i1 v1
#Description:Decrement items by 1 valid decrement. Returns items as an array on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Unsupported items will trigger failure. '' (an empty value) cannot be used in array, except for an empty array. This means "''" is valid, but "'', $value2, $value3" is not. Since '' is the lowest array possible, it cannot be decremented further, and will trigger an error if used as an input array. It can be returned on successful decrement though of an array 1 increment above.
#Arguments:'array' is an array containing supported items to decrement. 'item_list' is an array of supported items to decrement by. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):array:array:required,item_list:array:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_decrement_items_i1_v1') === FALSE){
function ritchey_decrement_items_i1_v1($array, $item_list, $display_errors = NULL){
	$errors = array();
	if (@empty($array) === TRUE){
		$errors[] = "array";
	} else if (@is_array($array) === FALSE){
		$errors[] = "array";
	}
	if (@empty($item_list) === TRUE){
		$errors[] = "item_list";
	} else if (@is_array($item_list) === FALSE){
		$errors[] = "item_list";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Reverse array
		$array = @array_reverse($array);
		###Reverse item_list
		$item_list = @array_reverse($item_list);
		###Decrement the first item that isn't already at item_list[0]. If all are, change all to item_list[0], and remove 1 value.
		$skip_check_if_entire_array_same = FALSE;
		$check_if_decremented = FALSE;
		foreach ($array as &$value) {
			if ($value === ''){
				$errors[] = "item_list (contains '' as value at key above [0].)";
				goto result;
			} else if (isset($value) === TRUE) {
				$check = FALSE;
				for ($i = 0; $i < count($item_list); $i++) {
					$ii = $i + 1;
					if ($value === $item_list[$i]) {
						if ($ii < count($item_list)){
							$value = $item_list[$ii];
							goto break_increment;
						} else {
							$value = $item_list[0];
						}
						$check = TRUE;
					}
				}
				if ($check === FALSE){
					$errors[] = "item_list (unsupported item:\"{$value}\")";
					goto result;
				}
			}
		}
		unset($value);
		break_increment:
		###Unreverse array
		$array = @array_reverse($array);
		###Check if entire array is $item_list[ends]'s. If it is remove last value to decrement next range.
		$end = @count($item_list);
		if ($skip_check_if_entire_array_same === FALSE){
			$check = FALSE;
			foreach ($array as &$value) {
				if ($value !== $item_list[0]){
					$check = TRUE;
				}
			}
			unset($value);
			if ($check === FALSE){
				$delete = @array_pop($array);
			}
		}
		###Check if entire array is empty, and if it is, add a value of '' to it since that is the lowest array possible.
		if (@empty($array) === TRUE){
			$array[] = '';
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('ritchey_decrement_items_i1_v1_format_error') === FALSE){
				function ritchey_decrement_items_i1_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("ritchey_decrement_items_i1_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $array;
	} else {
		return FALSE;
	}
}
}
?>